
@extends('master')
@section('content')
<script src="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="public/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="public/css/demo.css" />
<link rel="stylesheet" type="text/css" href="public/css/component.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.container{
    background: #D0F0C0 !important;
}
.box{
  background-color: #a3e8ac !important;
}
.inputfile-1 + label {
    color: #f1e5e6;
    background-color: darkslategrey;
}
.inputfile + label{
  padding: 0.625rem 13%;
}
.inputfile-1:focus + label, .inputfile-1.has-focus + label, .inputfile-1 + label:hover{
    background-color: darkslategrey;
}
</style>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
<script>
  function sum() {
      var txtFirstNumberValue = document.getElementById('girls').value;
      var txtSecondNumberValue = document.getElementById('boys').value;
      if (txtFirstNumberValue == "")
          txtFirstNumberValue = 0;
      if (txtSecondNumberValue == "")
          txtSecondNumberValue = 0;

      var result = parseInt(txtFirstNumberValue) + parseInt(txtSecondNumberValue);
      if (!isNaN(result)) {
          document.getElementById('total').value = result;
	document.getElementById('total1').value =result;
      }
  }
</script>
<script>
function getval(sel)
{
console.log("yay");
if(sel.value==8){
console.log("8");

document.getElementById("eig").style.display="block";
document.getElementById("nin").style.display="none";
}
else if(sel.value==9){
console.log("9");
document.getElementById("nin").style.display="block";
document.getElementById("eig").style.display="none";
}
}
function phonenumber(inputtxt)
{
  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(inputtxt.value.match(phoneno))
     {
	   return true;
	 }
   else
     {
	   alert("Not a valid Phone Number");
     inputtxt.value="";
	   return false;
     }
}
</script>
<script>
$('#my_file').change(function(){
    console.log()
    var filename = $(this).val().split('\\').pop();
});
</script>

    <h2>Teacher Post Submission</h2>
    <h5 style="text-align:right;font-weight:600;color:red;">* marked fields are compulsory</h5>

    <form method="post" action="posts" name="form1" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group row">
            <label for="titleid" class="col-sm-3 col-form-label">Name Of The School <b style="font-weight:600;color:red;"> * </b></label>
            <div class="col-sm-9">
                <input name="school" type="text" class="form-control" id="school" placeholder="Enter the name of your school" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="titleid" class="col-sm-3 col-form-label">Name Of The Teacher <b style="font-weight:600;color:red;"> * </b></label>
            <div class="col-sm-9">
                <input name="teacher" type="text" class="form-control" id="teacher" placeholder="Enter The Teachers Name" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="titleid" class="col-sm-3 col-form-label">Enter your mobile no.<b style="font-weight:600;color:red;"> * </b></label>
            <div class="col-sm-9">
                <input name="mobile" type="text" onfocusout="phonenumber(document.form1.mobile)" class="form-control" id="mobile" placeholder="Enter Your Mobile No." required>
            </div>
        </div>
        <div class="form-group row">
            <label for="publisherid" class="col-sm-3 col-form-label">Date on which Learning Unit is conducted <b style="font-weight:600;color:red;"> * </b></label>
            <div class="col-sm-9">
              <input type="text" id="datepicker" name="startdate" class="form-control input-md" placeholder="Enter Date" required="required">
            </div>
        </div>
          <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label">Student Count<b style="font-weight:600;color:red;"> * </b></label>
              </div>
              <div class="col-md-4">
                <div class="input-group">
                  <!-- <span class="input-group-addon">Girls</span> -->
                  <input id="girls" name="Girls" class="form-control" placeholder="Count of Girls" type="number" min=0 required onkeyup="sum()" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-group">
                  <!-- <span class="input-group-addon">Boys</span> -->
                  <input id="boys" name="Boys" class="form-control" placeholder="Count of Boys" type="number" min=0 required onkeyup="sum()" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-group">
                  <!-- <span class="input-group-addon">Total</span> -->
	           <input type="hidden" name="total" id="total" value="">
                  <input id="total1"  class="form-control" placeholder="Total" type="text" disabled required>
                </div>
              </div>
          </div>
          <div class="form-group row">
              <label for="gameimageid" class="col-sm-3 col-form-label">Upload Students List<b style="font-weight:600;color:red;"> * </b></label>
          </div>
          <div class="form-group row">
              <div class="col-sm-12">

                <div class="box">
                  <input type="file" class="inputfile inputfile-1" name="listof"  required  style="opacity: 0"  id="file-1"  data-multiple-caption="{count} files selected"  />
                  <!-- <input class="custom-file-input" name="listof" required id="listof" aria-describedby="inputGroupFileAddon01"> -->
                  <label for="file-1" ><svg xmlns="http://www.w3.org/2000/svg" width="40" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>

                <!-- <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                  </div>
                  <div class="custom-file">
                    <input class="custom-file-input" name="listof" required type="file" id="listof" aria-describedby="inputGroupFileAddon01">
                    <label id="filename" class="custom-file-label" for="inputGroupFile01">Choose file</label>
                  </div>
                </div> -->
              </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">Class in which unit is conducted<b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                  <select id="class" name="class" onchange="getval(this);" class="form-control">
                    <option value="8">8th</option>
                    <option value="9">9th</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">The Unit which was conducted <b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                  <select name="unit" class="form-control" id="eig">
                    <option value="Exploring area and perimeter of rectangles">  Exploring area and perimeter of rectangles</option>
                    <option value="Finding the right path">Finding the right path </option>
                    <option value="Euclids Game">Euclid’s Game</option>
                    <option value="Investigating Conditions for Congruence of Triangles Quadrilaterals and Polygons">Investigating Conditions for Congruence of Triangles, Quadrilaterals and Polygons</option>
                    <option value="Colors and Maps">Colors and Maps</option>
                    <option value="An experiment on measuring volumes">An experiment on measuring volumes</option>
                    <option value="Bringing back shine to metal">Bringing back shine to metal</option>
                    <option value="Diffusion across surfaces">Diffusion across surfaces	</option>
                    <option value="Force between magnets">Force between magnets</option>
                    <option value="Know your Fibers">Know your Fibers</option>
                    <option value="Osmosis in raisins">Osmosis in raisins</option>
                    <option value="Parallax and Reflection">Parallax and Reflection</option>
                    <option value="Pinhole camera">Pinhole camera</option>
                    <option value="Shadows">Shadows</option>
                    <option value="The journey from Milk to Curd">The journey from Milk to Curd</option>
                    <option value="Understanding adolescence and gender">Understanding adolescence and gender</option>
                    <option value="What the Moths Taught Us">What the Moths Taught Us</option>
                    <option value="Mapping the school campus">Mapping the school campus</option>
                    <option value="Discover Describe and Draw Birds">Discover, Describe and Draw Birds</option>
                    <option value="Micro-organisms on your doorstep">Micro-organisms on your doorstep</option>
                    <option value="Know your Rice">Know your Rice!</option>
                  </select>
                  <select name="unit" class="form-control" id="nin" style="display:none;">
                    <option value="Exploring Patterns in Square Numbers">  Exploring Patterns in Square Numbers</option>
                    <option value="An Exploration of Fractals">An Exploration of Fractals</option>
                    <option value="Exploring Irrational Numbers">Exploring Irrational Numbers</option>
                    <option value="Mid-Point Quadrilaterals">Mid-Point Quadrilaterals</option>
                    <option value="Biodiversity and Classification">Biodiversity and Classification</option>
                    <option value="Is there protein in those grains">Is there protein in those grains?</option>
                    <option value="The Accidental Discovery">The Accidental Discovery</option>
                    <option value="How “Z” got its name">How “Z” got its name</option>
                    <option value="Changes during evaporation">Changes during evaporation</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">What did students learn from this Unit? <b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                   <textarea class="form-control" id="learned" name="learned" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">Any A-ha moment while conducting the Learning Unit? <b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                   <textarea class="form-control" id="learned" name="learned" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">Interesting questions and comments by students <b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="questions" name="questions" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="publisherid" class="col-sm-3 col-form-label">Any feedback on unit <b style="font-weight:600;color:red;"> * </b></label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="feedback" name="feedback" required></textarea>
                </div>
            </div>
            <div class="form-group row">
              <label for="gameimageid" class="col-sm-12 col-form-label">Upload Photos</label>
            </div>
            <div class="form-group row" id="image1" style="display:none;">
                <div class="col-sm-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Upload 1st Image</span>
                    </div>
                    <div class="custom-file">
                      <input class="custom-file-input" type="file" accept="image/*" name="image1" id="photos" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" id="file-image-1" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" id="image2" style="display:none;">
                <div class="col-sm-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Upload 2nd Image</span>
                    </div>
                    <div class="custom-file">
                      <input class="custom-file-input" type="file" name="image2" accept="image/*" id="photos" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" id="file-image-2" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                </div>

              </div>
              <div class="form-group row" id="image3" style="display:none;">
                <div class="col-sm-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Upload 3rd Image</span>
                    </div>
                    <div class="custom-file">
                      <input class="custom-file-input" type="file"  name="image3" accept="image/*" id="photos" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" id="file-image-3" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" id="image4" style="display:none;">
                <div class="col-sm-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Upload 4th Image</span>
                    </div>
                    <div class="custom-file">
                      <input class="custom-file-input" type="file" name="image4" accept="image/*" id="photos" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" id="file-image-4" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" id="image5" style="display:none;">
                <div class="col-sm-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Upload 5th Image</span>
                    </div>
                    <div class="custom-file">
                      <input class="custom-file-input" type="file" name="image5" accept="image/*" id="photos" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" id="file-image-5" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12">
                  <div class="input-group">
                    <button type="button" id="buttoned" onClick="addbutton();">Click To Add Photos</button>
                  </div>
                </div>
              </div>
        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
<script>
var display=0;
function addbutton()
{
  document.getElementById("buttoned").innerHTML="Add More Photos";
  display++;
  if(display>5){
    display=5;
    document.getElementById("buttoned").disabled = true;
    document.getElementById("buttoned").innerHTML="Reached Upload Limit";
  }
  else{
    document.getElementById("buttoned").disabled = false;

  }
  if(display==5){
      document.getElementById("image5").style.display="block";
      document.getElementById("buttoned").disabled = true;
      document.getElementById("buttoned").innerHTML="Reached Upload Limit";
  }
  else if (display==4) {
    document.getElementById("image4").style.display="block";
  }
  else if(display==3){
    document.getElementById("image3").style.display="block";
  }
  else if(display==2){
    document.getElementById("image2").style.display="block";
  }
  else if(display==1){
    document.getElementById("image1").style.display="block";
  }
  else{
    console.log("error");
  }
}
</script>
<script src="public/js/custom-file-input.js"></script>
@endsection
