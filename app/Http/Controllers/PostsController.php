<?php

namespace App\Http\Controllers;
use App\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller
{
  public function index()
    {
        $posts = Posts::all();
        return view('posts.create');
    }

    public function show(Game $id)
    {
        return view('games.show', ['game' => $id]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function success()
    {

        return view('success');
    }

    public function store(Request $request)
   {
       $string = str_replace(' ', '-', request('school').request('startdate'));
       $new_file_name=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
       $post = new Posts;
        $post->school = request('school');
        $post->teacher= request('teacher');
        $post->mobile= request('mobile');
        $post->startdate= request('startdate');
        $post->Girls= request('Girls');
        $post->Boys= request('Boys');
        $post->total= request('total');
        $post->listof= request('listof')->getClientOriginalName();
        $post->image = request()->file('listof')->store('public/images/'.$new_file_name);
        $post->path= $new_file_name;
        $post->unit= request('unit');
        $post->class= request('class');
        $post->learned= request('learned');
        $post->questions= request('questions');
        $post->feedback= request('feedback');
        if(request('image1'))
        {
        $post->image1=request()->file('image1')->store('public/images/'.$new_file_name.'/blogpics');
        }
        if(request('image2'))
        {
        $post->image2=request()->file('image2')->store('public/images/'.$new_file_name.'/blogpics');
        }
        if(request('image3'))
        {
        $post->image3=request()->file('image3')->store('public/images/'.$new_file_name.'/blogpics');
        }
        if(request('image4'))
        {
        $post->image4=request()->file('image4')->store('public/images/'.$new_file_name.'/blogpics');
        }
        if(request('image5'))
        {
          $post->image5=request()->file('image5')->store('public/images/'.$new_file_name.'/blogpics');
        }
        $post->save();
        return view('posts.success');

   }
}
