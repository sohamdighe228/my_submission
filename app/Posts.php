<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $fillable = [
      'school', 'teacher', 'mobile', 'path','startdate', 'Girls', 'Boys', 'total', 'listof','unit', 'class','learned', 'questions', 'feedback', 'image1','image2','image3','image4','image5'
  ];


  protected $hidden = [
      'id',
  ];
}
