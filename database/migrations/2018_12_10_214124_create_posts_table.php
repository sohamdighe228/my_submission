<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school');
            $table->string('teacher');
            $table->string('mobile');
            $table->string('image');
            $table->string('startdate');
            $table->string('Girls');
            $table->string('Boys');
            $table->string('total');
            $table->string('listof');
            $table->string('path');
            $table->string('unit');
            $table->string('class');
            $table->string('learned');
            $table->string('questions');
            $table->string('feedback');
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('image5')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('posts');
      Schema::dropIfExists('image');
    }
}
